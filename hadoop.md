修改本地模式为参数接收

![image-20210701095846953](https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210701095846953.png)

![image-20210701100025680](https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210701100025680.png)

打jar包

![image-20210701100328168](https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210701100328168.png)

<img src="https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210701100414012.png" alt="image-20210701100414012" style="zoom: 67%;" />

![image-20210701100433735](https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210701100433735.png)

此时jar包已打好

<img src="https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210701100525327.png" alt="image-20210701100525327" style="zoom:50%;" />

重命名为clean.jar，并将jar包传入虚拟机

<img src="https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210701095906115.png" alt="image-20210701095906115" style="zoom:50%;" />

切换目录，执行数据清洗操作

hadoop jar clean.jar /JobData/20210701 /JobData/output

![image-20210701095228320](https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210701095228320.png)

运行中

![image-20210701095301197](https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210701095301197.png)

![image-20210701095327601](https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210701095327601.png)

完成

<img src="https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210701095504062.png" alt="image-20210701095504062" style="zoom: 67%;" />

结果![image-20210701095530771](https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210701095530771.png)

查看

![image-20210701110826096](https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210701110826096.png)



1. 采集 
   1. 数据采集类：HttpClientData
2. 清洗
   1. 在hadoop上进行清洗
   2. 清洗结果存在HDFS中
3. 数据分析
   1. 将结构化数据转成hive表
      - 表类型
        - 事实表：业务主体数据
        - 维度表：业务分析属性









1.创建新的hive数据库

![image-20210701102428962](https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210701102428962.png)



2.创建事实表

```sql
create table ods_jobdata_origin(city string comment '城市', salary array<string> comment '薪资', welfare array<string> comment '福利', skill array<string> comment '技能') comment '原始职位表' ROW FORMAT DELIMITED FIELDS TERMINATED BY ',' COLLECTION ITEMS TERMINATED BY '-' STORED AS TEXTFILE;
```

查看表结构

```sql
desc ods_jobdata_origin;
```

![image-20210701104100128](https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210701104100128.png)



3.导入数据到事实表

load data inpath '/JobData/output/part-r-00000' overwrite into table ods_jobdata_origin;

![image-20210701104354740](https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210701104354740.png)

![image-20210701111102150](https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210701111102150.png)



4.创建明细表，并加载数据

```sql
create table ods_jobdata_detail(city string comment '城市', salary array<string> comment '薪资', welfare array<string> comment '福利', skill array<string> comment '技能', law_salary int comment '低薪资', high_salary int comment '高薪资', avg_salary double comment '平均薪资') comment '职位明细表' ROW FORMAT DELIMITED FIELDS TERMINATED BY ',' COLLECTION ITEMS TERMINATED BY '-' STORED AS TEXTFILE;
```

![image-20210701105357536](https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210701105357536.png)

插入数据

```sql
insert overwrite table ods_jobdata_detail select city,salary,welfare,skill,salary[0],salary[1],(salary[0]+salary[1]/2) from ods_jobdata_origin;
```

![image-20210701111259233](https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210701111259233.png)

![image-20210701111340990](https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210701111340990.png)



5.创建中间表（目的是为了将数据扁平化-将数组的数据变为字段数据）

1.将薪资数据扁平化处理

```sql
create table t_ods_tmp_salary as select explode(ojo.salary) from ods_jobdata_origin ojo;
```

![image-20210701111553946](https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210701111553946.png)

<img src="https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210701111629263.png" alt="image-20210701111629263" style="zoom:50%;" />



2.将薪资数据泛化处理

```sql
create table t_ods_tmp_salary_dist as select case
when col >= 0 and col <= 5 then '0-5'
when col >= 6 and col <= 10 then '6-10'
when col >= 11 and col <= 15 then '11-15' 
when col >= 16 and col <= 20 then '16-20' 
when col >= 21 and col <= 25 then '21-25' 
when col >= 26 and col <= 30 then '26-30'
when col >= 31 and col <= 35 then '31-35' 
when col >= 36 and col <= 40 then '36-40' 
when col >= 41 and col <= 45 then '41-45' 
when col >= 46 and col <= 50 then '46-50'
when col >= 51 and col <= 55 then '51-55' 
when col >= 56 and col <= 60 then '56-60' 
when col >= 61 and col <= 65 then '61-65' 
when col >= 66 and col <= 70 then '66-70'
when col >= 71 and col <= 75 then '71-75' 
when col >= 76 and col <= 80 then '76-80' 
when col >= 81 and col <= 85 then '81-85' 
when col >= 86 and col <= 90 then '86-90'
when col >= 91 and col <= 95 then '91-95' 
when col >= 96 and col <= 100 then '0-100' 
when col >= 101 then '>101'
end from t_ods_tmp_salary;
```

![image-20210703162846343](https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210703162846343.png)

<img src="https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210701113059119.png" alt="image-20210701113059119" style="zoom:50%;" />



3.将福利数据扁平化

```sql
create table t_ods_tmp_welfare as select explode(ojo.welfare) from ods_jobdata_origin ojo;
```

![image-20210701113355948](https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210701113355948.png)



4.将技能数据扁平化

```sql
create table t_ods_tmp_skill as select explode(ojo.skill) from ods_jobdata_origin ojo;
```

![image-20210701113420012](https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210701113420012.png)



5.创建维度表（存储统计结果）

```sql
create table t_ods_salary(every_partition string comment '薪资分布', count int comment '词频') comment '薪资分布统计' ROW FORMAT DELIMITED FIELDS TERMINATED BY ',' STORED AS TEXTFILE;


create table t_ods_city(every_city string comment '城市分布', count int comment '词频') comment '城市分布统计' ROW FORMAT DELIMITED FIELDS TERMINATED BY ',' STORED AS TEXTFILE;


create table t_ods_welfare(every_welfare string comment '福利分布', count int comment '词频') comment '福利分布统计' ROW FORMAT DELIMITED FIELDS TERMINATED BY ',' STORED AS TEXTFILE;


create table t_ods_skill(every_skill string comment '技能分布', count int comment '词频') comment '技能分布统计' ROW FORMAT DELIMITED FIELDS TERMINATED BY ',' STORED AS TEXTFILE;
```

![image-20210703161432794](https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210703161432794.png)



6.数据分析

1） 职位区域分析

```sql
insert overwrite table t_ods_city select city,count(1) from ods_jobdata_origin group by city;
```

<img src="https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210703162031660.png" alt="image-20210703162031660"  />

<img src="https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210703162108044.png" alt="image-20210703162108044"  />



2） 薪资范围分析

```sql
insert overwrite table t_ods_salary select `_c0`,count(1) from t_ods_tmp_salary_dist group by `_c0`;
```

![image-20210703162504741](https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210703162504741.png)

select * from t_ods_salary;

![image-20210703163347153](https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210703163347153.png)



3） 公司福利分析

```sql
insert overwrite table t_ods_welfare select col,count(1) from t_ods_tmp_welfare group by col;
```

![image-20210703164200014](https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210703164200014.png)

![image-20210703164533687](https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210703164533687.png)

```sql
select * from t_ods_welfare sort by count desc limit 5;
```

![image-20210703164824510](https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210703164824510.png)



4） 技能要求分析

```sql
insert overwrite table t_ods_skill select col,count(1) from t_ods_tmp_skill group by col;
```

![image-20210703164934860](https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210703164934860.png)

![image-20210703165012738](https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210703165012738.png)

```sql
select * from t_ods_skill sort by count desc limit 3;
```

![image-20210703165210035](https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210703165210035.png)



5） 平均薪资

```sql
select avg(avg_salary) from ods_jobdata_detail;
```

![image-20210703165404618](https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210703165404618.png)



6） 各城市平均薪资

```sql
select city,count(city),round(avg(avg_salary),2) as cnt from ods_jobdata_detail group by city order by cnt desc; 
```

![image-20210703165737866](https://pic-img-21.oss-cn-hangzhou.aliyuncs.com/img/image-20210703165737866.png)