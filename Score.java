package Score;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class Score {
	public static void main(String[] args) throws FileNotFoundException, IOException {
		
		// 读取配置文件
	    Properties pf = new Properties();
	    pf.load(new FileInputStream("src\\Score.properties"));
	    
		// 解析这两个html文本
		File sf = new File("src\\small.html");
		File af = new File("src\\all.html");
		
		Document ds = Jsoup.parse(sf, "UTF-8");
		Document da = Jsoup.parse(af, "UTF-8");
		
		
		//Score  score=new Score();
		//HashMap<String,Integer>small=score.parseHtml(args[0]);
		//HashMap<String,Integer>small=score.parseHtml(args[1]);

		// 初始化个人经验
		/**
		 * before:课前自测
		 * base：课堂完成
		 * test：课堂小测
		 * program：编程题
		 * add：附加题
		 */
		int before = 0;
		int base = 0;
		int test = 0;
		int program = 0;
		int add = 0;
		double total_before = Integer.parseInt(pf.getProperty("before"));
		double total_base = Integer.parseInt(pf.getProperty("base"));
		double total_test = Integer.parseInt(pf.getProperty("test"));
		double full_program = Integer.parseInt(pf.getProperty("program"));
		double full_add = Integer.parseInt(pf.getProperty("add"));

		// 云班课经验统计部分
		if (ds != null) {
			Elements elements = ds.getElementsByAttributeValue("class", "interaction-row");
			int temp;
			for (int i = 0; i < elements.size(); i++) {
			//判断是哪种类型的活动和该活动的完成，可以使用 jsoup 中的 get()、child() 来定位到里面的孩子节点
			//找到经验值所在位置的相应节点分离出来，用 element.text() 的方法获取标签中的内容，再将内容放入 Scanner 中使用 nextDouble() 读出数字
			//将读出来的经验统计起来
				if (elements.get(i).child(1).child(0).toString().contains("课前自测")) {
					if (elements.get(i).child(1).child(2).toString().contains("color:#8FC31F")) {
						Scanner sc = new Scanner(
								elements.get(i).child(1).child(2).children().get(0).children().get(10).text());
						temp = sc.nextInt();
						before += temp;
					}
				} else if (elements.get(i).child(1).child(0).toString().contains("课堂完成")) {
					if (elements.get(i).child(1).child(2).toString().contains("已参与&nbsp;")) {
						Scanner sc = new Scanner(
								elements.get(i).child(1).child(2).children().get(0).children().get(7).text());
						temp = sc.nextInt();
						base += temp;
					}
				} else if (elements.get(i).child(1).child(0).toString().contains("课堂小测")) {
					if (elements.get(i).child(1).child(2).toString().contains("已参与&nbsp;")) {
						Scanner sc = new Scanner(
								elements.get(i).child(1).child(2).children().get(0).children().get(7).text());
						temp = sc.nextInt();
						test += temp;
					}
				} else if (elements.get(i).child(1).child(0).toString().contains("编程题")) {
					if (elements.get(i).child(1).child(2).toString().contains("已参与&nbsp;")) {
						Scanner sc = new Scanner(
								elements.get(i).child(1).child(2).children().get(0).children().get(7).text());
						temp = sc.nextInt();
						program += temp;
					}
				} else if (elements.get(i).child(1).child(0).toString().contains("附加题")) {
					if (elements.get(i).child(1).child(2).toString().contains("已参与&nbsp;")) {
						Scanner sc = new Scanner(
								elements.get(i).child(1).child(2).children().get(0).children().get(10).text());
						temp = sc.nextInt();
						add += temp;
					}
				} 
			}
		}
		if(da != null) {
			Elements elements = da.getElementsByAttributeValue("class", "interaction-row");
			int temp;
			for(int i = 0; i < elements.size(); i++) {
				if (elements.get(i).child(1).child(0).toString().contains("附加题")) {
					if (elements.get(i).child(1).child(2).toString().contains("已参与&nbsp;")) {
						Scanner sc = new Scanner(
								elements.get(i).child(1).child(2).children().get(0).children().get(10).text());
						temp = sc.nextInt();
						add += temp;
					}
				} 
			}
		}
		//经验值换算百分制成绩
		double result_before = before / total_before * 100* 0.25;
		double result_base = base / total_base * 100 * 0.3* 0.95;
		double result_test = test / total_test * 100 * 0.2;
		double result_program = program / full_program *100 * 0.1*0.95;
		double result_add = add / full_add * 100 * 0.05*0.9;
	    //最终求取所获得的经验值
		double total_score = (result_add + result_base + result_before + result_program + result_test)*0.9   + 6;
		System.out.println(String.format("%.2f", total_score));
		
		//static int getScore(String filepath) throws IOException {
		// File file = new File(filepath);
		// InputStream inputStream = null;
		// try {
		// inputStream = new FileInputStream(file);
		//
		// } catch (FileNotFoundException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// StringBuilder stringBuilder = new StringBuilder("");
		//
		// InputStreamReader ipr = new InputStreamReader(inputStream, "UTF-8");
		// BufferedReader br = new BufferedReader(ipr);
		// String line = null;
		// while ((line = br.readLine()) != null) {
		// stringBuilder.append(line + "\n");
		// if(line.contains("</html>")){
		// break;
		// }
		// }
		// String srt = stringBuilder.toString();
		// String h5[] = new String[100];
		// return 0;
		 //解析网页来获取每个模块的经验值
		
	}
}
